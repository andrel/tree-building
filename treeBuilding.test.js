const assert = require('assert');
const { rawNodes, buildTree } = require('./treeBuilding');

const result = buildTree(rawNodes);

const expected = [
  {
    node: {
      id: 'a',
      parent_id: null
    },
    children: [
      {
        node: {
          id: 'd',
          parent_id: 'a'
        },
        children: [
          {
            node: {
              id: 'e',
              parent_id: 'd'
            },
            children: [
              {
                node: {
                  id: 'f',
                  parent_id: 'e'
                },
                children: []
              }
            ]
          }
        ]
      }
    ]
  },
  {
    node: {
      id: 'b',
      parent_id: null
    },
    children: [
      {
        node: {
          id: 'g',
          parent_id: 'b'
        },
        children: []
      }
    ]
  },
  {
    node: {
      id: 'c',
      parent_id: null
    },
    children: []
  }
];

assert.deepEqual(result, expected);

/**
 * @typedef {Object} RawNode
 * @property {String} id
 * @property {String} parent_id
 */
const rawNodes = [
  {
    id: 'e',
    parent_id: 'd'
  },
  {
    id: 'a',
    parent_id: null
  },
  {
    id: 'b',
    parent_id: null
  },
  {
    id: 'c',
    parent_id: null
  },
  {
    id: 'd',
    parent_id: 'a'
  },
  {
    id: 'f',
    parent_id: 'e'
  },
  {
    id: 'g',
    parent_id: 'b'
  }
];

/**
 * @typedef {Object} Node
 * @property {Object} node
 * @property {Array<Node>} children
 */

/**
 * Transforms the raw node into the expected node format
 * @param {RawNode} node
 * @returns {Node}
 */
function buildNode(node) {
  const result = {
    node,
    children: []
  };

  return result;
}

/**
 * Builds the node tree
 * @param {Array<RawNode>} nodeList
 * @param {String} parent
 * @returns {Array<Node>}
 */
function buildTree(rawNodeList, parent = null) {
  // Sort alphabetically by `id`
  const sortedNodeList = rawNodeList.sort((a, b) => a.id > b.id);

  // Get all nodes at this level
  const nodes = sortedNodeList.filter(node => node.parent_id === parent);

  // Build the nodes at this level
  const result = nodes.reduce((acc, currNode) => {
    // Normalize node
    const treeNode = buildNode(currNode);

    // Check if the node has children
    const hasChildren = sortedNodeList.some(
      _node => _node.parent_id === treeNode.node.id
    );

    // If yes, then recursively get them in the original list,
    // passing the actual node id as the next parent id.
    // Otherwise the `children` property will be an empty array
    // that was added when building the node on `buildNode`
    if (hasChildren) {
      treeNode.children = buildTree(rawNodeList, treeNode.node.id);
    }

    return acc.concat(treeNode);
  }, []);

  return result;
}

module.exports = {
  rawNodes,
  buildTree
};
